{-# LANGUAGE ViewPatterns #-}   
{-# LANGUAGE OverloadedStrings #-}
module NbString
  (main

  ) where 
import qualified Data.ByteString.Char8 as BSC
import qualified Data.ByteString as BS
import Data.Maybe

json :: BSC.ByteString
json = "{ \"Name\" : \"Lasse\", \"Age\" : 33 , \"MyList\" :  [2 , \"Hej\", 11.44 , { \"InnerAdr\" : { \"StreetZip\" : \"123.33\"}, \"City\" : \"Malmoe\" }, [1, 44, \"Text\"]] , \"Anithyn\" : Null, \"Adress\" : { \"Street\" : \"Lassegatan\"}}"
test1 :: BSC.ByteString
test1 = " , \"MyList\" :  [2 , \"Hej\", 11.44 , { \"InnerAdr\" : { \"StreetZip\" : \"123.33\"}, \"City\" : \"Malmoe\" }, [1, 44, \"Text\"]] , \"Anithyn\" : Null, \"Adress\" : { \"Street\" : \"Lassegatan\"}}"
failJson :: BSC.ByteString
failJson = "dsdflndfjal"
nada :: BSC.ByteString
nada = "Nothing"

substring :: BSC.ByteString -> Int -> Int -> Maybe BSC.ByteString
substring (BS.uncons -> Nothing) _ _ = Nothing
substring str start stop =
  let strStr = BSC.drop start str in
  if strStr == BSC.empty then Nothing
  else Just (BSC.take ((stop + 1) - start) (BSC.drop start str))

getNestedString1 :: BSC.ByteString -> Maybe (BSC.ByteString, BSC.ByteString)
getNestedString1 (BS.uncons -> Nothing) = Nothing 
getNestedString1 str =
  let start = BSC.elemIndex '"' str in
      case start of
        Nothing -> Nothing
        Just startIdx -> let stop = BSC.elemIndex '"' (BSC.drop (startIdx + 1) str) in
          case stop of
            Nothing -> Nothing
            Just stopIdx -> Just (BSC.take takeLength  (BSC.drop dropLength str), BSC.drop (takeLength + dropLength +1) str)
              where takeLength = (stopIdx + startIdx) - startIdx
                    dropLength = startIdx + 1

getNestedString :: BSC.ByteString -> Maybe (BSC.ByteString, BSC.ByteString)
getNestedString (BS.uncons -> Nothing) = Nothing 
getNestedString str =
  BSC.elemIndex '"' str >>=
  (\start -> BSC.elemIndex '"' (BSC.drop (start + 1) str) >>=
             (\stop -> let takeLength = (stop + start) - start
                           dropLength = start + 1 in 
                       return (BSC.take takeLength (BSC.drop dropLength str), BSC.drop (takeLength + dropLength + 1) str)))

byStartClose :: BSC.ByteString -> Char -> Char -> Bool -> Maybe (BSC.ByteString, BSC.ByteString)
byStartClose (BS.uncons -> Nothing ) _ _ _ = Nothing
byStartClose str startChar endChar inclusive =
  let start = BSC.elemIndex startChar str in
    case start of
      Nothing -> Nothing
      Just startIndex -> let stop = byStartClose' str startChar endChar (startIndex + 1) 0 in
                             case stop of
                               Nothing -> Nothing
                               Just stopIndex -> let sub = substring str startIndex stopIndex in
                                                   case sub of
                                                     Nothing -> Nothing
                                                     Just substr -> Just (substr, BSC.drop stopIndex str)
                                   


byStartClose' :: BSC.ByteString -> Char -> Char -> Int -> Int -> Maybe Int
byStartClose' (BS.uncons -> Nothing ) _ _ _ _ = Nothing
byStartClose' str startChar endChar currentIndex numbOfNested =
  let currChar = BSC.index str currentIndex in
    if currChar == startChar then byStartClose' str startChar endChar (currentIndex + 1) (numbOfNested + 1)
    else if currChar == endChar then
           if numbOfNested > 0 then byStartClose' str startChar endChar (currentIndex + 1) (numbOfNested - 1)
           else Just currentIndex
    else byStartClose' str startChar endChar (currentIndex + 1) numbOfNested
  
              
         
      
main :: IO ()
main = putStrLn "jhds"
